import pygame, sys
import os
import pyganim, math
from pygame.locals import *

class Player(object):
    def __init__(self):
        self.WALKRATE = 4
        self.RUNRATE = 12

    def magnitude(v):
        return math.sqrt(sum(v[i]*v[i] for i in range(len(v))))

    def add(u, v):
        return [ u[i]+v[i] for i in range(len(u)) ]

    def sub(u, v):
        return [ u[i]-v[i] for i in range(len(u)) ]

    def dot(u, v):
        return sum(u[i]*v[i] for i in range(len(u)))

    def normalize(v):
        vmag = magnitude(v)
        return [ v[i]/vmag  for i in range(len(v)) ]



class Briar(Player,object):
    def __init__(self):
        front_standing = pygame.image.load('images/briar_front3.png')
        back_standing = pygame.image.load('images/briar_back3.png')
        right_standing = pygame.image.load('images/briar_right3.png')
        left_standing = pygame.transform.flip(right_standing, True, False)
        self.x, self.y = (0,0)
        self.set_target((0, 0))

    def move(self):
        animTypes = 'front back right'.split()
        animObjs = {}

        for animType in animTypes:
            imagesAndDurations = [('images/briar_%s%s.png' % animType,str(num), 0.1) for num in range(10)]

        #create the left-facing sprites by copying and flipping the right-facing sprites
        animObjs['left'] = animObjs['right'].getCopy()
        animObjs['left'].flip(True, False)
        animObjs['left'].makeTransformsPermanent()


        return animObjs

    @property
    def pos(self):
        return self.x, self.y

    # for drawing, we need the position as tuple of ints
    # so lets create a helper property
    @property
    def int_pos(self):
        return map(int, self.pos)

    @property
    def target(self):
        return self.t_x, self.t_y

    @property
    def int_target(self):
        return map(int, self.target)

    def set_target(self, pos):
        self.t_x, self.t_y = pos

    def update(self):
        # if we won't move, don't calculate new vectors
        if self.int_pos == self.int_target:
            return

        target_vector = Player.sub(self.target, self.pos)

        # a threshold to stop moving if the distance is to small.
        # it prevents a 'flickering' between two points
        if Player.magnitude(target_vector) < 2:
            return

        # apply the players's speed to the vector
        move_vector = [c * self.speed for c in Player.normalize(target_vector)]

        # update position
        self.x, self.y = Player.add(self.pos, move_vector)

    #def draw(self, s):
        #pygame.draw.circle(s, (255, 0 ,0), self.int_pos, 2)

class Tris:
    def __init__(self):
        front_standing = pygame.image.load('images/tris_front3.png')
        back_standing = pygame.image.load('images/tris_back3.png')
        right_standing = pygame.image.load('images/briar_right3.png')
        left_standing_standing = pygame.transform.flip(right_standing, True, False)

    def move(self):
        animTypes = 'front back right'.split()
        animObjs = {}

        for animType in animTypes:
            imagesAndDurations = [('images/briar_%s%s.png' % animType,str(num), 0.1) for num in range(10)]

        #create the left-facing sprites by copying and flipping the right-facing sprites
        animObjs['left'] = animObjs['right'].getCopy()
        animObjs['left'].flip(True, False)
        animObjs['left'].makeTransformsPermanent()

        moveConductor = pyganim.PygConductor(animObjs)

        return moveConductor

class Daja:
    def __init__(self):
        front_standing = pygame.image.load('images/briar_front3.png')
        back_standing = pygame.image.load('images/briar_back3.png')
        right_standing = pygame.image.load('images/briar_right3.png')
        left_standing_standing = pygame.transform.flip(right_standing, True, False)

    def move(self):
        animTypes = 'front back right'.split()
        animObjs = {}

        for animType in animTypes:
            imagesAndDurations = [('images/briar_%s%s.png' % animType,str(num), 0.1) for num in range(10)]

        #create the left-facing sprites by copying and flipping the right-facing sprites
        animObjs['left'] = animObjs['right'].getCopy()
        animObjs['left'].flip(True, False)
        animObjs['left'].makeTransformsPermanent()

        moveConductor = pyganim.PygConductor(animObjs)

        return moveConductor

class Sandry:
    def __init__(self):
        front_standing = pygame.image.load('images/briar_front3.png')
        back_standing = pygame.image.load('images/briar_back3.png')
        right_standing = pygame.image.load('images/briar_right3.png')
        left_standing_standing = pygame.transform.flip(right_standing, True, False)

    def move(self):
        animTypes = 'front back right'.split()
        animObjs = {}

        for animType in animTypes:
            imagesAndDurations = [('images/briar_%s%s.png' % animType,str(num), 0.1) for num in range(10)]

        #create the left-facing sprites by copying and flipping the right-facing sprites
        animObjs['left'] = animObjs['right'].getCopy()
        animObjs['left'].flip(True, False)
        animObjs['left'].makeTransformsPermanent()

        moveConductor = pyganim.PygConductor(animObjs)

        return moveConductor

