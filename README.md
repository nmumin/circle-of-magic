Adventure Game in the classic point-and-click style, inspired by the amazing Lucas Arts game of ye olden days.

Game will be written in Python, probably using PyGame.

Games will be based off and/or inspired by the Emelan-verse of fantasy author Tamora Pierce.