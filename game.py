# main game file
# eventually will disperse to separate files, but for testing this is fine


import pygame
from pygame.locals import *
import sys
import time
import pyganim


pygame.init()

# define some constants
UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'

# set up the window
WINDOWWIDTH = 640
WINDOWHEIGHT = 480
MAPHEIGHT = 317
MAPWIDTH = WINDOWWIDTH
windowSurface = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), 0, 32)
pygame.display.set_caption('Circle of Magic')

backdrop = pygame.image.load('images/sotat317.png')
actions = pygame.image.load('images/inventor.png')



# load the "standing" sprites (these are single images, not animations)
front_standing = pygame.image.load('images/briar/briar_back_stand.png')
back_standing = pygame.image.load('images/briar/briar_front_stand.png')
right_standing = pygame.image.load('images/briar/briar_right_stand.png')
left_standing = pygame.transform.flip(right_standing, True, False)



playerWidth, playerHeight = front_standing.get_size()

animTypes = 'front back right'.split()
animObjs = {}

for animType in animTypes:
    imagesAndDurations = [('images/briar/briar_%s%s.png' % (animType,str(num)), 0.1) for num in range(10)]
    animObjs[animType] = pyganim.PygAnimation(imagesAndDurations)

#create the left-facing sprites by copying and flipping the right-facing sprites
animObjs['left'] = animObjs['right'].getCopy()
animObjs['left'].flip(True, False)
animObjs['left'].makeTransformsPermanent()


# have the animation objects managed by a conductor.
# With the conductor, we can call play() and stop() on all the animtion
# objects at the same time, so that way they'll always be in sync with each
# other.


### EVENTUALLY MOVE THIS CODE TO A 'PLAYERS' FILE
moveConductor = pyganim.PygConductor(animObjs)


direction = DOWN # player starts off facing down (front)

BASICFONT = pygame.font.Font('freesansbold.ttf', 16)
WHITE = (255, 255, 255)
BGCOLOR = (100, 50, 50)

mainClock = pygame.time.Clock()
x = 150 # x and y are the player's position
y = 200
WALKRATE = 4
RUNRATE = 12


# instructionSurf = BASICFONT.render('Arrow keys to move. Hold shift to run.', True, WHITE)
# instructionRect = instructionSurf.get_rect()
# instructionRect.bottomleft = (10, WINDOWHEIGHT - 10)

running = moveUp = moveDown = moveLeft = moveRight = False
mouseX, mouseY = pygame.mouse.get_pos()


graph = [line.split(',') for line in open("mapgraph.txt")] #this is the text based walkable mapping.

#move to buttons file later
close = pygame.image.load('images/buttons/close.png')
closeRect = close.get_rect()
closeRect.bottomleft = (1, 433 + 44)

opens = pygame.image.load('images/buttons/open.png')
openRect = opens.get_rect()
openRect.bottomleft = (1, 384 + 44)

give = pygame.image.load('images/buttons/give.png')
giveRect = give.get_rect()
giveRect.bottomleft = (1, 339+44)

while True:
    windowSurface.blit(backdrop, (0, 0))
    windowSurface.blit(close, closeRect)
    windowSurface.blit(give, giveRect)
    windowSurface.blit(opens, openRect)
    windowSurface.blit(actions, (0, 0))
    for event in pygame.event.get(): # event handling loop

        # handle ending the program
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                pygame.quit()
                sys.exit()

            if event.key in (K_LSHIFT, K_RSHIFT):
                # player has started running
                running = True

            if event.key == K_UP:
                moveUp = True
                moveDown = False
                if not moveLeft and not moveRight:
                    # only change the direction to up if the player wasn't moving left/right
                    direction = UP
            elif event.key == K_DOWN:
                moveDown = True
                moveUp = False
                if not moveLeft and not moveRight:
                    direction = DOWN
            elif event.key == K_LEFT:
                moveLeft = True
                moveRight = False
                if not moveUp and not moveDown:
                    direction = LEFT
            elif event.key == K_RIGHT:
                moveRight = True
                moveLeft = False
                if not moveUp and not moveDown:
                    direction = RIGHT

        elif event.type == KEYUP:
            if event.key in (K_LSHIFT, K_RSHIFT):
                # player has stopped running
                running = False

            if event.key == K_UP:
                moveUp = False
                # if the player was moving in a sideways direction before, change the direction the player is facing.
                if moveLeft:
                    direction = LEFT
                if moveRight:
                    direction = RIGHT
            elif event.key == K_DOWN:
                moveDown = False
                if moveLeft:
                    direction = LEFT
                if moveRight:
                    direction = RIGHT
            elif event.key == K_LEFT:
                moveLeft = False
                if moveUp:
                    direction = UP
                if moveDown:
                    direction = DOWN
            elif event.key == K_RIGHT:
                moveRight = False
                if moveUp:
                    direction = UP
                if moveDown:
                    direction = DOWN
        # elif event.type == MOUSEBUTTONUP:
        #         newX, newY = pygame.mouse.get_pos()
        #         if(graph[((newY+playerHeight)/30)-1][(newX/32)-1] == '1'):
        #             x = newX
        #             y = newY-playerHeight
        #             if(newY > mouseY):
        #                 #moveUp = True
        #                 #moveDown = False
        #                 if not moveLeft and not moveRight:
        #                         direction = UP
        #             if(newY < mouseY):
        #                 if not moveLeft and not moveRight:
        #                         direction = DOWN
        #             if(newX < mouseX):
        #                     if not moveDown and not moveUp:
        #                         direction = LEFT
        #             if(newX > mouseX):
        #                     if not moveDown and not moveUp:
        #                         direction = RIGHT
        #         mouseX, mouseY = pygame.mouse.get_pos()


    if moveUp or moveDown or moveLeft or moveRight:
        # draw the correct walking/running sprite from the animation object
        moveConductor.play() # calling play() while the animation objects are already playing is okay; in that case play() is a no-op
        if running:
            if direction == UP:
                animObjs['back'].blit(windowSurface, (x, y))
            elif direction == DOWN:
                animObjs['front'].blit(windowSurface, (x, y))
            elif direction == LEFT:
                animObjs['left'].blit(windowSurface, (x, y))
            elif direction == RIGHT:
                animObjs['right'].blit(windowSurface, (x, y))
        else:
            # walking
            if direction == UP:
                animObjs['back'].blit(windowSurface, (x, y))
            elif direction == DOWN:
                animObjs['front'].blit(windowSurface, (x, y))
            elif direction == LEFT:
                animObjs['left'].blit(windowSurface, (x, y))
            elif direction == RIGHT:
                animObjs['right'].blit(windowSurface, (x, y))


        # actually move the position of the player
        if running:
            rate = RUNRATE
        else:
            rate = WALKRATE

        if moveUp:
            if (graph[((y-rate+playerHeight)/30)-1][(x/32)-1] == '2'):
                print 'room exit'
            if (graph[((y-rate+playerHeight)/30)-1][(x/32)-1] == '1'):
                y -= rate
        if moveDown:
            if (graph[(((y+playerHeight+rate)/30)-1)][((x/32)-1)] == '2'):
                print 'room exit'
            if (graph[(((y+playerHeight+rate)/30)-1)][((x/32)-1)] == '1'):
                y += rate
        if moveLeft:
            if (graph[((y+playerHeight)/30)-1][((x-rate)/32)-1] == '2'):
                print 'room exit'
            if (graph[((y+playerHeight)/30)-1][((x-rate)/32)-1] == '1'):
                x -= rate
        if moveRight:
            if (graph[((y+playerHeight)/30)-1][((x+rate)/32)-1] == '2'):
                print 'room exit'
            if (graph[((y+playerHeight)/30)-1][((x+rate)/32)-1] == '1'):
                x += rate

    else:
        # standing still
        moveConductor.stop() # calling stop() while the animation objects are already stopped is okay; in that case stop() is a no-op
        if direction == UP:
            windowSurface.blit(front_standing, (x, y))
        elif direction == DOWN:
            windowSurface.blit(back_standing, (x, y))
        elif direction == LEFT:
            windowSurface.blit(left_standing, (x, y))
        elif direction == RIGHT:
            windowSurface.blit(right_standing, (x, y))

    # make sure the player doesn't move off the screen
    if x < 0:
        x = 0
    if x > WINDOWWIDTH - playerWidth:
        x = WINDOWWIDTH - playerWidth
    if y < 0:
        y = 0
    if y > MAPHEIGHT - playerHeight:
        y = MAPHEIGHT - playerHeight

    #windowSurface.blit(instructionSurf, instructionRect)

    pygame.display.update()
    mainClock.tick(30) # Feel free to experiment with any FPS setting.

    #def collision: